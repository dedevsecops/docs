# Naming Things is Easy

## Personal Access Tokens or API keys

A token or key's name should point to the git repo or script that uses that token. For example, if we had a pipeline in this repo that required a token, we would name the token "git@gitlab.com:dedevsecops/docs.git".
