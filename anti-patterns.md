# Anti-patterns

## Overview

An anti-pattern is a common response to a recurring problem that is usually ineffective and risks being highly counterproductive. Don't use these processes or products. They are to be avoided.

## List of anti-patterns

I'm going to steal the avoid/prefer format from [PRISM ⚡ BREAK](https://prism-break.org/en/), because the words "avoid" and "prefer" and somewhat non-confrontational and I'm not a super-genius - some of you will have valid reasons for following the patterns you follow, but if you want a truly decentralized platform, this is my advice:

| Avoid | Prefer | Why? |
| --- | --- | --- |
| kube2iam, kiam, [IAM Roles for Service Accounts (IRSA)](https://docs.aws.amazon.com/eks/latest/userguide/iam-roles-for-service-accounts.html) | Creating AWS IAM Users for each service with specific permissions, and using AWS access keys for authorization. | kube2iam and kiam are effectively deprecated by IRSA, and all three of those options only work if your Kubernetes nodes are running on AWS. If you have nodes running on any other cloud or on bare metal, this will not work - it's not cloud-agnostic. As a bonus, you no longer have to use iptables in a kube2iam sidecar to mangle access to your nodes. | 
| github.com | gitlab.com | While Github is currently the most popular git repository provider, there's no reason that it should be. GitLab, for example, is free for unlimited developers with unlimited public and private repos, and you can self-host it, so it's decentralized. I'm not saying that Gitlab is the best provider out there, but it's got a lot going for it that Github doesn't. |
| AWS RDS and other proprietary DB-as-a-Service | deploying actual open source databases with their official or community supported Helm charts | I've got a whole document about this one: [Why don't we use Amazon RDS database?](https://gitlab.com/dedevsecops/docs/-/blob/master/why_dont_we_use_aws_rds.md) |
| `helm --suppress-secrets` | Show changes to secrets | Suppressing secrets just hides changes that you should know about. It doesn't actual reveal the secret values. |
| `#!/usr/bin/env bash` | `#!/bin/bash` | `#!/usr/bin/env bash` is less predictable and less secure than just stating the full path to the bash executable. See conversation [here](https://stackoverflow.com/questions/16365130/what-is-the-difference-between-usr-bin-env-bash-and-usr-bin-bash). |
| Don't run containers as root | `runAsNonRoot: true` | Root bad. Durr. |
| Don't allow writes to container filesystem | `readOnlyRootFilesystem: true` | Containers should be immutable. Writing should only happen on attached volumes. This will significantly increase security. |
| Don't use `latest` or no tag | Use a specific tag | To ensure that you have repeatable builds and can easily troubleshoot issues. |
