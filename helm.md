# Helm

[Helm](https://helm.sh/) is "the package manager for Kubernetes."

Before we do any work with Helm, we SHOULD read or review these pages:

* Documentation: https://helm.sh/docs/
* Chart Best Practices Guide: https://helm.sh/docs/chart_best_practices/
* Cheatsheet: https://helm.sh/docs/intro/cheatsheet/
* Community charts: https://artifacthub.io/

We SHOULD install applications using a community chart from Artifact Hub if one exists. If the community chart is not acceptable or is not being maintained, we SHOULD create an issue, fork the community chart, fix it, then create a pull request with our solution.

If there is no community chart available, we SHOULD create our own chart following the Chart Best Practices Guide linked above, with the following modifications:

* Amendment to "Naming Conventions": for Pod `env:` (or ConfigMap `data:` that's used to define shell environment variables) we SHOULD follow the de facto shell naming convention of `SCREAMING_SNAKE_CASE` (alphanumeric words separated with underscores.) Otherwise, we have to hard code camelcase to screaming snake case translations in our Helm templates, which is extra code, interpolation, and possiblilty for errors.

If the chart we create is potentially useful to other organizations, we SHOULD open source it, and add it to Artifact Hub.

When deploying applications with Helm using GitOps, we SHOULD NOT use local charts (ie. `chart: .` or `chart: some_local_dir`). If we combine chart development and deployment in a gitops-style repo, we can't separate work on Helm chart bugs without triggering deployments.

Helm charts SHOULD be packaged using [SemVer](https://semver.org/) ([per the Chart Best Practices Guide](https://helm.sh/docs/chart_best_practices/conventions/#version-numbers)), and SHOULD be published to an [OCI-based registry](https://helm.sh/docs/topics/registries/#use-hosted-registries). OCI-based registries are preferred over an HTTP registry, because ["sharing a common storage standard that's not specific to Helm allows greater interoperability between tools from the wider container ecosystem for security, identity and access management, and more."](https://helm.sh/blog/storing-charts-in-oci/)
