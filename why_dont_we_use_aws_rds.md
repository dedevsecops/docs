# Why don't we use Amazon RDS database? 

## Overview

I am constantly asked this question, so I’m documenting the answer so I don’t have to waste time answering it anymore.

## Conventions

I talk about Amazon specifically in this document, but most of this applies to any proprietary cloud provider. The same arguments are true of Google Cloud, Azure, etc.

For the sake of simplicity, I’m going to refer to Amazon RDS as “RDS”, and I’m going to refer to PostgreSQL installed on a Kubernetes cluster via the Helm chart as “PostgreSQL”.

“AZ” refers to availability zones. Anything that isn’t multi-AZ isn’t really highly available, so we’re not going to talk about single-AZ configurations.

“HA” is highly available, meaning that the system has no single point of failure. This does not mean that it’s 100% uptime. It just means that it’s configured to recover very quickly if any single AZ, instance, or other point fails.

## Price

TL;DR: at our current utilization level, it’s much less expensive to run PostgreSQL than RDS.

AWS pricing for compute is per hour, and I’m converting to monthly, so you’ll see things like $0.036 * 24h (hours) * 30d (days) = the monthly price.

AWS pricing for storage is per GB per month, so it’s just $0.1 * 1GB = monthly price.

The PostgreSQL architecture has an additional multiplier of 2i (instances), because I’m comparing the minimum viable HA multi-AZ architecture.

All pricing is for multi-AZ configuration with reserved instances, because anything less than that is not really HA.

Amazon’s pricing between single-AZ and multi-AZ shows double the price, so I’m giving AWS the benefit of the doubt regarding it’s storage pricing and assuming that it’s talking about database size, not total storage size (ie. a 1GB database is 1GB storage, even though a multi-AZ setup would require at least 2GB to store a second copy).

Sources:

https://aws.amazon.com/rds/postgresql/pricing/

https://aws.amazon.com/ec2/pricing/reserved-instances/pricing/

https://aws.amazon.com/ebs/pricing/

We’ll start with the absolute smallest, an t3.micro with only 1GB of storage:

| | RDS | PostgreSQL |
| --- | --- | --- |
| Compute | $0.013 x 24h x 30d = $9.36 | $0.007 x 24h x 30d x 2i = $10.08 |
| Storage | $0.23 x 1GB = $0.23 | $0.1 x 1GB x 2i = $0.20 |
| Total | $9.59 | $10.28 |

And here’s a larger database using m5.large with 100GB storage:

| | RDS | PostgreSQL |
| --- | --- | --- |
| Compute | $0.114 x 24h x 30d = $82.08 | $0.61 x 24h x 30d x 2i = $87.84 |
| Storage | $0.23 x 100GB = $23.00 | $0.1 x 100GB x 2i = $20.00 |
| Total | $105.08 | $107.84 |

It seems, initially, that using RDS is slightly cheaper. But what this fails to take into account is real-world workloads, and specifically the revolution of containerization. You don’t need to pay for a dedicated instance for your database that will idle at 5% utilization all day. We can run 29 pods on a m5.large instance in our Kubernetes cluster and any of those pods can take full advantage of the large CPU and RAM resources on its instance. This ensures that we’re getting a good value from the compute resources we already have, because we’re deploying many low-utilization things to a single instance instead of having dedicated nodes for each of them. THIS IS ALMOST AN ORDER OF MAGNITUDE LESS EXPENSIVE! So, essentially, at our current utilization levels and probably for the next year, we can ignore the Compute line in the PostgreSQL price above, because we’re already paying for Kubernetes nodes to run all of our other pods and the PostgreSQL doesn’t add much utilization at all. So, at current utilization, the smallest database that RDS offers (which probably isn’t large enough to serve production traffic) would cost $9.59 per month, and a PostgreSQL database (that could expand to use an m5.large instance’s resources when necessary) would effectively cost $0.20 because we’re already paying for the compute for the Kubernetes cluster.

We have almost 40 database instances across our 7 Kubernetes clusters, so even if we assume that we can get by with the smallest database RDS offers, that’s around $400/month to switch to RDS. Most of our clusters are at 3 nodes already, so even if we removed the databases from the cluster, we would not be able to reduce the number of instances without impacting the HA of our other distributed services like Kafka that need 2 out of 3 nodes to remain HA (so if one AZ goes down, we have to have at least 3 AZs to keep 2 of 3.)

## Free (Libre) Open Source Software

RDS is Service as a Software Substitute (SaaSS), which means we have very little insight into it. The only interfaces we have are the web console and AWS CLI. We cannot do anything unless there’s a button in the UI or an option in the CLI for it. If we want more visibility into it or need help with an error message, we have to contact AWS support and wait for them - we are not empowered to troubleshoot on our own. We have no idea what it’s technical architecture looks like. We just have to trust that because it’s Amazon, it must be good… right?

Conversely, with PostgreSQL, it’s all open source running on compute resources that are fully under our control. If we need to implement a custom solution, we can. If we get an error message, we can look at the source code to see the actual logic that’s producing it. We are empowered to solve our own problems, because we have complete transparency into the compute resources, the docker images, the code, etc. There’s also a massive community for open source software, which means we can get usually find a solution quickly just by searching forums or asking on a community Slack channel.

More reasons why free/libre open source software is important:

* https://en.wikipedia.org/wiki/Free_software
* https://en.wikipedia.org/wiki/Open-source_software

## Decentralization

RDS is run by Amazon, which means:

* if there’s a problem with our bill, we have downtime
* if there’s a dispute about the terms of service, we have downtime
* it might be difficult to sign an agreement with an organization that competes with Amazon
* it might be difficult to service a country that bans Amazon (interesting that there are no AWS data centers in China outside of the special admin region of Hong Kong)
* we might not be able to adequately serve regions where AWS doesn’t have a presence (there are no AWS servers on the entire continent of Africa, and only one datacenter in all of South America)

None of these are currently real problems. And all of these affect our current infrastructure because all of our Kubernetes clusters are running on AWS EKS and EC2 instances - but they don’t have to be… One could argue that using Facebook isn’t really problematic. Maybe centralization isn’t really a problem. Maybe all of this blockchain stuff and talk about decentralization is just a bunch of cypherpunks making a big fuss about nothing. It seems crazy to continue to default to centralized platforms when we can just as easily practice what we preach and use free, open source decentralized alternatives.

## Duplication of Effort

Right now, we are getting really good at Kubernetes-native architecture. We’re figuring out HA, compliance, disaster recovery, security, authorization and access control, encryption everywhere, CI/CD pipelines, etc. A lot of work is going into solving this for the Kubernetes environments. When we add another infrastructure layer like AWS services, we have to figure all of this out for that new layer as well. Specifically for RDS, we have to figure out networking, security groups, AWS roles and policies to determine which pods have access to which RDS instances, etc. Admittedly, security in the current Kubernetes environment is pretty flat, you’re basically either an admin or you don’t have access, but the framework is there to build upon.

## Maintenance, Upgrades, Support, etc.

I often hear the argument that RDS automates all your problems away. I would retort by saying that it obfuscates it all away. All I get is a button that says “upgrade”. If it fails, I have no idea why and am powerless to do anything. I have to file a ticket, wait, complain, and then figure out how to do a restore myself. I’ll probably get a lot of insincere apologies, but I won’t get a refund. This will all take hours to days to resolve and I will never learn the real reason that it failed in the first place. And perhaps one could say, “well, they’re Amazon, it won’t fail, it will just work, because they have a team of world-class DBA geniuses creating that automation!” Uh huh. Well, the 19,200,000 results when you google “rds upgrade failed” are evidence that the AWS engineers are, in fact, fallible humans like the rest of us. There’s some comedy gold in those results too. My favorite one so far is this one:

https://www.iheavy.com/2015/02/12/is-upgrading-rds-like-a-shit-storm-that-will-not-end/

I laughed, I cried, I’ve had similar experiences with AWS.

The people who created PostgreSQL, which RDS is merely a wrapper for, are the real DBA geniuses, because they wrote the database that is used all over the world that Amazon chose as the basis for their RDS offering. I’ll gladly take 30 minutes to read their upgrade notes and do it myself with full control over the process and transparency into what’s happening over an enticing but occasionally diabolic “upgrade” button.

## Responsibility

Some people think only a fool would host even a byte of customer data on their own infrastructure, but others are comfortable with it. I don't know where this fear comes from... If you lose customer data and say, "we use RDS, it was Amazon's fault!", the customer isn't going to care - you've still lost that customer. As a person who's been managing data and databases for around 20 years, I have been through both real and simulated disaster recovery scenarios for Microsoft SQL, Oracle, MySQL/MariaDB, PostgreSQL, and some managed solutions that are essentially just wrappers around the same products. Running databases internally is easier, gives more control, is less expensive, and aligns nicely with our goal of decentralization.

## Self-sovereignty

When AWS makes changes to their `*aaS` platform, you cannot opt out. When they make a breaking change that requires work on your end, you have to do it. Amazon becomes your boss - Amazon affects your workload. With self-hosted FOSS solutions, nothing changes or has to change until you are ready. Generally speaking, this is not an issue - Amazon makes reasonable decisions on what and when it changes its platform, but the fact remains that they make the decision and you *must* follow it or change platforms.

## Imposter Syndrome

Many people thing that hosting databases is something that requires an elite team of database administrators and sre/devops/sysadmin folks. Well, it does if you're doing extremely custom or highly performant configurations, sure. But for most cases, people just need a database that answers using PostgreSQL protocols and queries and they need it to be up most of the time. Everybody thinks they need a new fully-loaded Ford F150 Raptor, when a used base-level Toyota Tacoma is typically more than adequate to do the job. The irony is that RDS doesn't do extremely custom configurations, and if you trust them with massive, highly performant workloads, you better hold them to the same standard that you would hold an internal team to. They will not help you design and optimize your data schema, they will not analyze and react to unusual performance alerts for your particular database instance. Their teams support their *platform*, not your individual instance on that platform. You will *still* need a team of experts to do anything complex, at which point, why wouldn't you listen to your experts and let them create a database configuration that's customized to your particular needs?

## Competition

If you are creating PaaS or SaaS products, Amazon is your competitor. Once you get enough marketshare, they will come for you. For example, Amazon has decided to dabble in the [distributed ledger space](https://www.forbes.com/sites/biserdimitrov/2020/02/25/no-blockchain-needed-digital-asset-integrates-with-amazon-web-services/#32b0188837f3)... Are they trying to complete with Ethereum now? I guess we can close up shop. I trust that their team of experts will make an amazing ledger without all the slow, miserable decentralization and confusing, complex blockchain backend. Maybe they’ll create Amazon DeFi next? `:trollface:`

It makes no sense to give your current or future competitor money and market share. This is what decentralization is all about! This is the revolution! If you don't care about decentralization, then just keep doing your [Facebook/Apple/Amazon/Netflix/Google](https://en.wikipedia.org/wiki/Big_Tech) thing. Pick a team, go all in on their platform, and keep hoping that you've made the right decision. But if you're interested in decentralization, then you know that we're here to promote: protocols, not platforms; source code, not products; supportive communities, not enterprise licensing.

## Conclusion

I did my time with physical servers, I did VMs, I did clouds, I’m currently loving Kubernetes-native as my base layer to work in - I don’t want to go back to clouds just like you don’t want to go back to PHP - I want to go forward to some kind of amazing new architecture that runs a decentralized “mist” of p2p Ethereum Virtual Machines.
