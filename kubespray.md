# Kubespray

## Overview

Kubespray is incredibly flexible, and it seems like everyone sets it up a little differently. This is an attempt to create a complete guide to set it up from scratch in AWS with ingress, cert-manager, Helm, and as much security as possible.

## Why Kubespray?

* Provides the latest version of Kubernetes: EKS and kops are still on 1.12 as of this writing.
* More popular than kops and eksctl: googling "kubernetes kubespray" returns more results than "kubernetes kops" and "kubernetes eksctl". Granted, googling "kubernetes eks" returns the most results, but EKS will never be cloud agnostic, so we should avoid it.
* Most decentralized: works on all major cloud providers as well as bare metal.
* It's really flexible! This is actually kind of a problem because some parts of it are not well documented, but that's what we're attempting to solve here.

## Why not Kubespray?

* Coworkers will all groan and ask why you didn't just use eksctl to create an EKS cluster.
* Kubeconfig file grants god-like powers - EKS with aws-iam-authenticator is more granular.
* By default Kubespray creates like 2 bastions, 3 masters, and 3 etcd instances. That's 8 friggin t2.mediums. That's $308.32/month, but EKS is only about $144/month. HALF PRICE! I thought Kubespray was cheaper, but I was wrong.

## Architecture

This document suggests these architectural choices:

* One cluster per deployment environment, with a strong preference for only two clusters: dev and prod. The dev cluster should be a sandbox where developers have complete autonomy. It can be used to test dev code as well as Kubernetes cluster upgrades and changes before they are performed on the prod cluster. The prod cluster should have limited access and high stability.

* One set of clusters per tenant. A tenant could be a team, a customer, or something else - whatever makes sense as a barrier that contains access and permissions to this set of clusters.

## Prerequisites

* Linux operating system (none of this has been tested on OS X)

* AWS Account with AWS CLI set up using Named Profiles. See the "Quick Configuration and Multiple Profiles" section in [Configuring the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) as well as [Named Profiles](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html). While not all of us have to managed multiple AWS accounts, using Named Profiles makes it much easier for those of us that do!

* Github account with SSH access enabled.

## Set up environment

You need to export some local shell environment variables to get this process started.

* AWS_DEFAULT_REGION and AWS_PROFILE are common [AWS environment variables](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html).

* CLUSTER_NAME is the name of your cluster like: `tenant-env`. For example, you could create two clusters named "tenant-dev" and "tenant-prod". Note, this document goes through the process of creating one cluster. If you are going to have a -dev and -prod, you need to follow this process twice.

* GITHUB_ORG is the name of your Github Organization. This is where you will create a copy of the kubespray git repository.

For example:
```bash
# Do not copy and paste this - you have to replace the values first.
export AWS_DEFAULT_REGION=us-east-2
export AWS_PROFILE=dedevsecops
export CLUSTER_NAME=tenant-dev
export DOMAIN_NAME=dedevsecops.com
export GITHUB_ORG=dedevsecops
```

## Duplicate the Kubespray repo

Based on https://help.github.com/en/articles/duplicating-a-repository

1. Create a new Github repo named "${CLUSTER_NAME}-kubespray".
1. Clone the kubespray repo and push it to your new repo:
   ```bash
   git clone --bare https://github.com/kubernetes-sigs/kubespray.git
   cd kubespray.git
   git push --mirror "git@github.com:${GITHUB_ORG}/${CLUSTER_NAME}-kubespray.git"
   cd ..
   rm -rf kubespray.git
   ```
1. Clone your new repo:
   ```bash
   git clone "git@github.com:${GITHUB_ORG}/${CLUSTER_NAME}-kubespray.git"
   ```
1. Add official kubespray repo as an upstream remote so you can [sync](https://help.github.com/en/articles/syncing-a-fork) it to pull upstream changes:
   ```bash
   git remote add upstream https://github.com/kubernetes-sigs/kubespray.git
   ```

## Create `env.sh` script

This will create a script that will setup your environment and add the SSH key to your keyring. You can source this script and it ensure that your aws (CLI), terraform, ansible, kubectl, and helm are all configured to work with this cluster. This script is idempotent, so you can source it repeatedly without causing any problems.

To create the script:

1. Change directory to your new repo.
1. Create a `env.sh` script in the repo root directory so you can quickly source env vars:
   ```bash
   cat <<EOF > env.sh
   #!/bin/bash
   
   # Export KUBESPRAY_DIR to the location of this script
   # Based on https://www.ostricher.com/2014/10/the-right-way-to-get-the-directory-of-a-bash-script/
   export KUBESPRAY_DIR="\$( cd "\$( dirname "\${BASH_SOURCE[0]}" )" && pwd )"

   # Export everything else
   export AWS_DEFAULT_REGION="${AWS_DEFAULT_REGION}"
   export AWS_PROFILE="${AWS_PROFILE}"
   export CLUSTER_NAME="${CLUSTER_NAME}"
   export DOMAIN_NAME="${DOMAIN_NAME}"
   export HELM_HOME="\${HOME}/.helm-${CLUSTER_NAME}"
   export KUBECONFIG="\${HOME}/.kube/${CLUSTER_NAME}.conf"
   export TF_VAR_AWS_SSH_KEY_NAME="${CLUSTER_NAME}"
   
   # Add SSH key
   ssh-add "\${HOME}/.ssh/${CLUSTER_NAME}.pem"

   # Add path to Helm
   export PATH="\${PATH}:\${HOME}/linux-amd64/"
   EOF
   ```

To source the script after it has been created:

```bash
source env.sh
```

## Create AWS infrastructure with Terraform

Based on https://github.com/MeridioRE/kubespray/blob/master/contrib/terraform/aws/README.md

1. Source the `env.sh` script.
1. Create a keypair (TODO: should probably encrypt this key...):
   ```bash
   aws ec2 create-key-pair --key-name "$CLUSTER_NAME" | jq -r .KeyMaterial > "${HOME}/.ssh/${CLUSTER_NAME}.pem"
   chmod 600 "${HOME}/.ssh/${CLUSTER_NAME}.pem"
   ```
1. Source the `env.sh` again to add the SSH key you just created.
1. Create a bucket for the Terraform code. This will fail if the bucket name already exists:
   ```bash
   if [[ $AWS_DEFAULT_REGION != 'us-east-1' ]]; then
     aws s3api create-bucket \
       --bucket "${CLUSTER_NAME}-tf" \
       --create-bucket-configuration LocationConstraint="$AWS_DEFAULT_REGION" \
       --region "$AWS_DEFAULT_REGION"
   else
     aws s3api create-bucket \
       --bucket "${CLUSTER_NAME}-tf"
   fi
   ```
1. Configure bucket versioning:
   ```bash
   aws s3api put-bucket-versioning \
     --bucket "${CLUSTER_NAME}-tf" \
     --versioning-configuration Status=Enabled
   ```
1. Create backend.tf file:
   ```bash
   echo "terraform {
     backend \"s3\" {
       bucket = \"${CLUSTER_NAME}-tf\"
       key    = \"${CLUSTER_NAME}\"
       region = \"${AWS_DEFAULT_REGION}\"
     }
   }" > "${KUBESPRAY_DIR}/contrib/terraform/aws/backend.tf"
   ```
1. Set cluster name:
   ```bash
   sed -i "s/aws_cluster_name = \"devtest\"/aws_cluster_name = \"${CLUSTER_NAME}\"/" "${KUBESPRAY_DIR}/contrib/terraform/aws/terraform.tfvars"
   ```
1. Use m5d.large instances for workers instead of t2.medium, because they have NVMe SSD instance storage, which will alleviate "disk pressure" warnings if you attach it to /var/lib/docker:
   ```bash
   sed -i "s/aws_kube_worker_num = 4/aws_kube_worker_num = 2/" "${KUBESPRAY_DIR}/contrib/terraform/aws/terraform.tfvars"
   sed -i 's/aws_kube_worker_size = "t2.medium"/aws_kube_worker_size = "m5d.large"/' "${KUBESPRAY_DIR}/contrib/terraform/aws/terraform.tfvars"
   ```
1. Remove variables that are incompatible with AWS Named Profiles (TODO: submit a PR to the main kubespray repo for this):
   ```bash
   sed -i '/access_key = "${var.AWS_ACCESS_KEY_ID}"/d' "${KUBESPRAY_DIR}/contrib/terraform/aws/create-infrastructure.tf"
   sed -i '/secret_key = "${var.AWS_SECRET_ACCESS_KEY}"/d' "${KUBESPRAY_DIR}/contrib/terraform/aws/create-infrastructure.tf"
   sed -i '/region     = "${var.AWS_DEFAULT_REGION}"/d' "${KUBESPRAY_DIR}/contrib/terraform/aws/create-infrastructure.tf"
   ```
1. Remove more variables. I'm too lazy to figure out sed for this right now. Just remove these blocks from `"${KUBESPRAY_DIR}/contrib/terraform/aws/variables.tf"` (TODO: submit a PR to the main kubespray repo for this):
   ```terraform
   variable "AWS_ACCESS_KEY_ID" {
     description = "AWS Access Key"
   }
   variable "AWS_SECRET_ACCESS_KEY" {
     description = "AWS Secret Key"
   }
   variable "AWS_DEFAULT_REGION" {
     description = "AWS Region"
   }
   ```
1. Based on [this](https://github.com/kubernetes-sigs/aws-ebs-csi-driver/blob/master/docs/example-iam-policy.json), edit `"${KUBESPRAY_DIR}/contrib/terraform/aws/modules/iam/main.tf"`, ensuring that `resource "aws_iam_role_policy" "kube-worker"` has this:
   ```json
           {
             "Effect": "Allow",
             "Action": [
               "ec2:CreateSnapshot",
               "ec2:CreateTags",
               "ec2:CreateVolume",
               "ec2:DeleteSnapshot",
               "ec2:DeleteTags",
               "ec2:DeleteVolume"
             ],
             "Resource": "*"
           },
   ```
1. To use the NVMe SSD instance storage of the m5d.large for /var/lib/docker, you need to edit `"${KUBESPRAY_DIR}/contrib/terraform/aws/create-infrastructure.tf"`. This is based on the "Use attached storage for Docker" section [here](https://coreos.com/os/docs/2079.6.0/mounting-storage.html), and the [Terraform Ignition Provider](https://www.terraform.io/docs/providers/ignition/index.html). Essentially you need to add some data objects and add a user_data attribute in the k8s-worker aws_instance. A diff should look like this when you're done:
   ```diff
   +data "ignition_filesystem" "ephemeral1" {
   +    name = "ephemeral1"
   +    mount {
   +        device = "/dev/nvme1n1"
   +        format = "ext4"
   +        wipe_filesystem = true
   +    }
   +}
   +
   +data "ignition_systemd_unit" "var_lib_docker_mount" {
   +  name = "var-lib-docker.mount"
   +  content = "[Unit]\nDescription=Mount ephemeral to /var/lib/docker\nBefore=local-fs.target\n[Mount]\nWhat=/dev/nvme1n1\nWhere=/var/lib/docker\nType=ext4\n[Install]\nWantedBy=local-fs.target"
   +}
   +
   +data "ignition_systemd_unit" "docker_service" {
   +  name = "docker.service"
   +  dropin {
   +    name = "10-wait-docker.conf"
   +    content = "[Unit]\nAfter=var-lib-docker.mount\nRequires=var-lib-docker.mount"
   +  }
   +}
   +
   +data "ignition_config" "use_attached_storage" {
   +  filesystems = [
   +    "${data.ignition_filesystem.ephemeral1.id}"
   +  ]
   +  systemd = [
   +    "${data.ignition_systemd_unit.var_lib_docker_mount.id}",
   +    "${data.ignition_systemd_unit.docker_service.id}"
   +  ]
   +}
   +
    resource "aws_instance" "k8s-worker" {
   +  user_data = "${data.ignition_config.use_attached_storage.rendered}"
   +
   ```
1. Change to Terraform directory:
   ```bash
   cd "${KUBESPRAY_DIR}/contrib/terraform/aws"
   ```
1. Initialize Terraform. This will fail if you already have a .terraform dir:
   ```bash
   terraform init
   ```
1. Create the infrastructure for your cluster:
   ```bash
   terraform apply
   ```
1. Commit and push your changes:
   ```bash
   git add -A
   git commit -m 'Adding env.sh and Terraform updates.'
   git push
   ```
1. Wait a few minutes for EC2 instances to pass all status checks.

## Create Kubernetes cluster with Ansible

Based on https://github.com/kubernetes-sigs/kubespray/blob/master/README.md

1. Source the `env.sh` script.
1. Install requirements. Added -H because of the error "The directory '/home/evans/.cache/pip/http' or its parent directory is not owned by the current user and the cache has been disabled. Please check the permissions and owner of that directory. If executing pip with sudo, you may want sudo's -H flag.":
   ```bash
   sudo -H pip install -r "${KUBESPRAY_DIR}/requirements.txt"
   ```
1. Copy the sample inventory:
   ```bash
   cp -rfp "${KUBESPRAY_DIR}/inventory/sample" "${KUBESPRAY_DIR}/inventory/mycluster"
   ```
1. Make a symbolic link EXACTLY LIKE THIS. If you copy, move, or otherwise link this wrong, horrible, nightmarish things will happen to your cluster: when you run `terraform apply`, it will destroy everything you've built; when you run `ansible-playbook` it will freak out becahse it can't find any hosts, and if you run the playbook on `inventory/hosts` instead of `inventory/mycluster/hosts`, it won't recognize any of your group_vars variables. Failing to do this step consumed three fucking days of my life:
   ```bash
   cd "${KUBESPRAY_DIR}/inventory/mycluster/"
   ln -s ../hosts
   ```

> RANDOM INLINE WARNING: I was skimming the variables in `"${KUBESPRAY_DIR}/inventory/mycluster/group_vars/all/all.yml"` and I thought it would be reasonable to set `cloud_provider: aws` in there because I'm using AWS. DO NOT DO THIS! IT'S A TRAP! I think it's some kind of Oracle OCI bullshit - I don't know what it is, but if you set it, none of your nodes or masters will be able to see each other and it will fuck everything up. I am going to `git blame` the person who created this variable name and track them down and go up to them and be like... "HEY! That was not a very good name! It's really generic! It should be called shitty_oracle_oci_cloud_thing_that_will_reck_your_shit_n00b" Yeah. That's what I'm gonna do.

1. Based on [this](https://github.com/kubernetes-sigs/aws-ebs-csi-driver/tree/master/docs#prerequisites), add this setting to `"${KUBESPRAY_DIR}/inventory/mycluster/group_vars/k8s-cluster/k8s-cluster.yml"`:
   ```bash
   echo -e 'kube_feature_gates:\n  - VolumeSnapshotDataSource=true' >> "${KUBESPRAY_DIR}/inventory/mycluster/group_vars/k8s-cluster/k8s-cluster.yml"
   ```
1. Ensure these settings in `"${KUBESPRAY_DIR}/inventory/mycluster/group_vars/k8s-cluster/addons.yml"` (changing ingress_nginx_host_network due to [this](https://kubernetes.slack.com/archives/C2V9WJSJD/p1559740335023800)):
   ```yaml
   sed -i 's/helm_enabled: false/helm_enabled: true/' "${KUBESPRAY_DIR}/inventory/mycluster/group_vars/k8s-cluster/addons.yml"
   sed -i 's/ingress_nginx_enabled: false/ingress_nginx_enabled: true/' "${KUBESPRAY_DIR}/inventory/mycluster/group_vars/k8s-cluster/addons.yml"
   sed -i 's/# ingress_nginx_host_network: false/ingress_nginx_host_network: true/' inventory/mycluster/group_vars/k8s-cluster/addons.yml
   sed -i 's/cert_manager_enabled: false/cert_manager_enabled: true/' "${KUBESPRAY_DIR}/inventory/mycluster/group_vars/k8s-cluster/addons.yml"
   echo -e 'tiller_enable_tls: true\ntiller_secure_release_info: true' >> "${KUBESPRAY_DIR}/inventory/mycluster/group_vars/k8s-cluster/addons.yml"
   ```
1. Update .gitignore so you can actually save your config:
   ```bash
   echo 'inventory/mycluster/credentials' > "${KUBESPRAY_DIR}/.gitignore"
   ```
1. Commit and push your changes:
   ```bash
   git add -A
   git commit -m 'Adding inventory/mycluster, ssh-bastion.conf, and updating .gitignore.'
   git push
   ```
1. If you have a poor connection or you're creating a cluster in remote datacenter (e.g., creating a cluster in eu-west-3 from your laptop in the U.S.A.), then skip to [this](#using_bastion) section.
1. If you have a stable connection to a local datacenter, then change to the `"${KUBESPRAY_DIR}"` directory and create the cluster:
   ```bash
   cd "${KUBESPRAY_DIR}"
   ansible-playbook -i ./inventory/mycluster/hosts ./cluster.yml -e ansible_user=core -b --become-user=root --flush-cache
   ```

## Using bastion to create cluster<a name="using_bastion"></a>

If you try to use Ansible to create a cluster in a far away datacenter, you may run into issues. For example, running Ansible from a laptop in Portland, Oregon, USA to create a Kubernetes cluster in the eu-west-3 datacenter in Paris, France will take 30 minutes and will probably fail many, MANY times due to timeouts... even if you have a really awesome gigabit fiber Internet connection. To fix that issue:

Based on https://coreos.com/os/docs/latest/install-debugging-tools.html

1. Source the `env.sh` script.
1. Log into a bastion server:
   ```bash
   ssh core@$(grep 'bastion.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2 | head -n 1)
   ```
1. Add a user to SSH directly into the toolbox:
   ```bash
   sudo useradd bob -m -p '*' -s /usr/bin/toolbox -U -G sudo,docker,rkt
   sudo passwd bob
   ```
1. Disconnect from the bastion and log back in as user bob:
   ```bash
   ssh bob@$(grep 'bastion.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2 | head -n 1)
   ```
1. Create a .ssh directory:
   ```bash
   mkdir .ssh
   ```
1. Inside the toolbox, install some utilities:
   ```bash
   dnf -y install python3-pip git
   ```
1. TODO: this is a bad idea - you don't want to leave this shit laying around - they should be password protected. Disconnect from the bastion and copy your Github SSH and the cluster SSH keys to the bob account:
   ```bash
   scp "${HOME}/.ssh/id_rsa" bob@$(grep 'bastion.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2 | head -n 1):/root/.ssh/
   scp "${HOME}/.ssh/${CLUSTER_NAME}.pem" bob@$(grep 'bastion.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2 | head -n 1):/root/.ssh/
   ```
1. Log back into the bob user:
   ```bash
   ssh bob@$(grep 'bastion.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2 | head -n 1)
   ```
1. Clone your repo:
   ```bash
   git clone "git@github.com:${GITHUB_ORG}/${CLUSTER_NAME}-kubespray.git"
   ```
1. Setup an SSH agent:
   ```bash
   eval $(ssh-agent)
   ```
1. Change to the repo directory.
1. Source the `env.sh` file.
1. Install requirements:
   ```bash
   pip3 install -r "${KUBESPRAY_DIR}/requirements.txt"
   ```
1. Run the Ansible playbook to create the cluster:
   ```bash
   ansible-playbook -i ./inventory/mycluster/hosts ./cluster.yml -e ansible_user=core -b --become-user=root --flush-cache
   ```

## Download kubeconfig file

TODO: Might be able to use the kubeconfig_localhost option instead of doing all this.

1. Source the `env.sh` script.
1. Copy the remote kube config file to your `"${HOME}/.kube"` directory (and create the directory if it doesn't exist):
   ```bash
   mkdir -p "${HOME}/.kube"
   ssh -F "${KUBESPRAY_DIR}/ssh-bastion.conf" core@$(grep 'master0.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2) -- sudo cat /etc/kubernetes/admin.conf > "${HOME}/.kube/${CLUSTER_NAME}.conf"
   ```
1. Get the load balancer name:
   ```bash
   export LB_NAME=$(aws elb describe-load-balancers | jq -r ".LoadBalancerDescriptions[] | select(.DNSName|startswith(\"kubernetes-elb-${CLUSTER_NAME}\")) | .DNSName")
   ```
1. Replace the IP address in your `"${HOME}/.kube/${CLUSTER_NAME}.conf"` with the load balancer DNS name:
   ```bash
   sed -i "s#    server: .*#    server: https://${LB_NAME}:6443#" "${HOME}/.kube/${CLUSTER_NAME}.conf"
   ```

## Install Helm

You need to install the Helm version specified in `"${KUBESPRAY_DIR}/roles/download/defaults/main.yml"`.

1. Source the `env.sh` file.
1. Get the correct version of Helm:
   ```bash
   cd "${HOME}"
   export helm_package=helm-v2.13.1-linux-amd64.tar.gz
   export helm_checksum=c1967c1dfcd6c921694b80ededdb9bd1beb27cb076864e58957b1568bc98925a
   curl -O "https://get.helm.sh/${helm_package}"
   if [[ $(sha256sum "${helm_package}" | cut -d' ' -f1) == "${helm_checksum}" ]]; then
     tar -xzvf "${helm_package}"
     rm "${helm_package}"
   fi
   ```
1. Install Helm diff plugin:
   ```bash
   mkdir -p "${HELM_HOME}/plugins"
   helm plugin install https://github.com/databus23/helm-diff --version master
   ```

## Download Helm certificates

1. Source the `env.sh` script.
1. Copy Helm certificates to artifacts directory:
   ```bash
   ssh -F "${KUBESPRAY_DIR}/ssh-bastion.conf" core@$(grep 'master0.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2) -- sudo cp -r /root/.helm /home/core/
   ssh -F "${KUBESPRAY_DIR}/ssh-bastion.conf" core@$(grep 'master0.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2) -- sudo chown -R core /home/core/.helm
   scp -F "${KUBESPRAY_DIR}/ssh-bastion.conf" -r core@$(grep 'master0.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2):/home/core/.helm/*.pem "${HELM_HOME}"
   ssh -F "${KUBESPRAY_DIR}/ssh-bastion.conf" core@$(grep 'master0.*ansible_host' "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2) -- sudo rm -rf /home/core/.helm
   ```
 
## Create AWS EBS CSI storage class

Based on https://github.com/kubernetes-sigs/aws-ebs-csi-driver/blob/master/docs/README.md

1. Install AWS EBS CSI:
   ```bash
   kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-ebs-csi-driver/master/deploy/kubernetes/manifest.yaml
   ```
1. Create an ebs-sc.yaml file like this:
   ```bash
   cat <<EOF > ebs-sc.yaml
   kind: StorageClass
   apiVersion: storage.k8s.io/v1
   metadata:
     annotations:
       "storageclass.kubernetes.io/is-default-class": "true"
     name: ebs-sc
   parameters:
     encrypted: "true"
   provisioner: ebs.csi.aws.com
   volumeBindingMode: WaitForFirstConsumer
   EOF
   ```
1. Create an encrypted storageclass:
   ```bash
   kubectl apply -f ebs-sc.yaml
   ```

You can now use helm with the `--tls` argument like this:
```bash
helm ls --tls
```

## external-dns

Based on https://github.com/kubernetes-incubator/external-dns/blob/master/docs/tutorials/aws.md

TODO: Put this in Terraform after we get the bugs worked out of this whole stack.

1. Create a zone:
   ```bash
   aws route53 create-hosted-zone --name "${CLUSTER_NAME}.${DOMAIN_NAME}." --caller-reference "${CLUSTER_NAME}-$(date +%s)"
   ```

1. Get the name servers:
   ```bash
   export ZONE_ID=$(aws route53 list-hosted-zones-by-name --output json --dns-name "${CLUSTER_NAME}.${DOMAIN_NAME}." | jq -r '.HostedZones[0].Id')
   aws route53 list-resource-record-sets --output json --hosted-zone-id "${ZONE_ID}" \
    --query "ResourceRecordSets[?Type == 'NS']" | jq -r '.[0].ResourceRecords[].Value'
   ```
1. Log into whatever service you use to manage DNS for `${DOMAIN_NAME}`, and create multiple NS records for the cluster's subdomain (in this example `${CLUSTER_NAME}.${DOMAIN_NAME}`) - one for each NS record that's output by the command above.
1. Do not proceed until you see all four nameservers from your local machine:
   ```bash
   dig +short "${CLUSTER_NAME}.${DOMAIN_NAME}" NS
   ```
1. See additional code in the helmfile directory, then use helmfile install external-dns:
   ```bash
   helmfile apply
   ```

## How to connect to the cluster via SSH

Source the `env.sh` script.

How to log into master0:
```bash
ssh -F "${KUBESPRAY_DIR}/ssh-bastion.conf" core@$(grep master0.*ansible_host "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2)
```
From a master, you can run kubectl with sudo:
```bash
sudo kubectl get nodes
```
How to log into worker0:
```bash
ssh -F "${KUBESPRAY_DIR}/ssh-bastion.conf" core@$(grep worker0.*ansible_host "${KUBESPRAY_DIR}/inventory/mycluster/hosts" | cut -d= -f2)
```
