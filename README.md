# DeDevSecOps

## Overview

The Decentralized (De) Software Development (Dev), Information Security (Sec), and Information Technology Operations (Ops) group was created to collaborate on code and documentation for "production-ready", "enterprise grade" infrastructure with these features:

### Community Supported

Prefer software that has a higher numbers of active developers, commits, and issues opened and closed. Active communities will find and fix bugs, and add features faster and more elegantly than less active communities. Check out [CNCF](https://landscape.cncf.io/license=open-source&project=hosted,graduated,incubating,sandbox,member) software when designing architecture, because they keep track of the fastest growing, most current, open source solutions so you don't have to. Also, it's easier to find helpful articles about popular software.

### Compliant

Prefer architectural choices that are compatible with SOC 2, PCI, CIS, ISO 27001, HIPAA, etc. 

### Decentralized

Prefer self-hostable services, and try to be cloud/service agnostic when possible, because one day you may be global in scale, and your cloud service or licensing might not be available in your customers' country.

### Documented

Document everything completely, because code is useless if no one can figure out how to use it...

### Free (Gratis) Software

Prefer free (as in beer) software, because no one likes asking for approval or filling out expense reports. Also, if you're a startup, you probably have a really tight budget, but you can always afford software that's $0.

### Free (Libre) Open Source Software

Prefer libre open source software, because sometimes you have to fork the code and fix it yourself. Also troubleshooting things is much easier when you can just read the code.

### Gitops

It would be ideal to have a totally declarative architecture where a `git push` triggerred any and all changes. Your git log would provide a perfect, auditable history of all changes to your environments. Keep in mind that this is an ideal, and probably not practicle. Still, 80% gitops is better than no gitops.

### Highly Available (HA)

Prefer highly available configurations by default, because single points of failure are bad. Also, if your dev environment is HA, then you can test your HA setup before your implement it in production.

### Horizontally Scalable

Prefer solutions that are horizontally scalable, because you can almost always add more nodes, but you can't always add bigger nodes. For reference, as of June 2019, the largest AWS instances have a maximum of either 4TB memory or 349 compute units. 

### Secure by default

Default to encryption in transit, encryption at rest, principle of least privilege, and enable audit logging. Avoid anonymous access and default passwords.

### Stable

Avoid alpha versions of software.

## Get Started

[Kubespray](kubespray.md)
